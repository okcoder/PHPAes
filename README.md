# PHPAes

#### 介绍
Aes加密加密类,兼容PHP7



#### 安装教程

```
composer require okcoder/php-aes
```

#### 使用说明

> PHP

```
$key = 'abcdef1234567890';
$iv = '0987654321fedcba';
$aes = new \OkCoder\PHPAes\Main($key,$iv);

$aes->encode('okcoder'); # t5PQWBIMdKymy2uLgrSOXQ==

$aes->decode('t5PQWBIMdKymy2uLgrSOXQ==');　＃　okcoder
```

> JS

先 ```npm install crypto-js``` 然后把下载的文件拿来使用

```
// aes.js

let CryptoJS = require('./CryptoJS/crypto-js'); //引用之前下载的源码

const key = CryptoJS.enc.Utf8.parse(CryptoJS.MD5("abcdef1234567890").toString().substr(0, 16));
const iv = CryptoJS.enc.Utf8.parse(CryptoJS.MD5("0987654321fedcba").toString().substr(0, 16));

/**
 * 解密
 * @param word string 密文(已base64加密的密文)
 * @returns {string}　返回明文
 */
function decode(word) {
    return CryptoJS.AES.decrypt(word, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
}

/**
 * 加密
 * @param word string 明文
 * @returns {string}  返回密文(已base64加密的密文)
 */
function encode(word) {
    let srcs = CryptoJS.enc.Utf8.parse(word);
    let encrypted = CryptoJS.AES.encrypt(srcs, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
}

//暴露接口
module.exports = {encode, decode}


```


#### 赞助二维码

![微信打赏](https://images.gitee.com/uploads/images/2018/1228/122201_32d7aaa0_2159240.png "微信.png")
![支付宝打赏](https://images.gitee.com/uploads/images/2018/1228/122220_5d93117e_2159240.png "支付宝.png")
